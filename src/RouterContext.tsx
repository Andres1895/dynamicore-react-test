import React, { createContext, useContext } from 'react';
import { Location } from 'history';
import { useLocation } from 'react-router-dom';

interface RouterContextType {
  location: Location;
}
const RouterContext = createContext<RouterContextType | undefined>(undefined);

export const useRouter = () => {
  const context = useContext(RouterContext);
  if (!context) {
    throw new Error('useRouter must be used within a RouterProvider');
  }
  return context;
};

// Context provider component
export const RouterProvider = ({ children }: any) => {
  const location = useLocation();

  return (
    <RouterContext.Provider value={{ location }}>
      {children}
    </RouterContext.Provider>
  );
};
