import { BrowserRouter as Router } from "react-router-dom";
import Tabs from './Tabs'

const App = () => {
  return (
    <Router>
      <Tabs/>
    </Router>
  );
};

export default App;
