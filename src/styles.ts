import styled from "styled-components";
import { Link } from "react-router-dom";

export const Nav = styled.nav`
  display: flex;
  justify-content: center;
  width: 100%;
`;

export const Button = styled.button`
  background-color: transparent;
  cursor: pointer;
  height: 2rem;
  margin: 1rem;
  min-width: 7rem;
  width: auto;
  border: none;
`;

export const StyledLink = styled(Link)<{ isActive: Boolean }>`
  color: black;
  text-decoration: none;
  border-bottom: ${(props) => (props.isActive ? "2px solid #607998" : "none")};
`;
