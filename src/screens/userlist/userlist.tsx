import { Users as users } from "./names.text";
import { UserList } from "../../components";
import { Container } from "./styles";

const UserListScreen = () => {
  return (
    <Container>
      <UserList users={users} />{" "}
      {/* users is the list of user to pass as props in UserList*/}
    </Container>
  );
};

export default UserListScreen;
