import { useState } from "react";
import { useForm } from "react-hook-form";
import { FormContainer, StyledForm, SubmittedForm, Label, ErrorLabel, Input } from "./styles";

type FormType = {
  name: string;
  email: string;
};

const Form = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormType>();
  const [submittedData, setSubmittedData] = useState<FormType>();

  const onSubmit = (data: FormType) => {
    setSubmittedData(data);
  };

  return (
    <FormContainer>
      <StyledForm onSubmit={handleSubmit(onSubmit)}>
        <Input>
          <label>Nombre:</label>
          <input
            type="text"
            {...register("name", { required: "El nombre es obligatorio" })}
          />
          {errors.name && <ErrorLabel>{errors.name.message}</ErrorLabel>}
        </Input>
        <Input>
          <label>Email:</label>
          <input
            type="email"
            {...register("email", {
              required: "El email es obligatorio",
              pattern: {
                value: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
                message: "El formato del email es incorrecto",
              },
            })}
          />
          {errors.email && <ErrorLabel>{errors.email.message}</ErrorLabel>}
        </Input>
        <button type="submit">Enviar</button>
      </StyledForm>
      <SubmittedForm isVisible={submittedData !== undefined}>
        <h3>Datos Ingresados:</h3>
        <Label>Nombre: {submittedData?.name}</Label>
        <Label>Email: {submittedData?.email}</Label>
        <button onClick={() => setSubmittedData(undefined)}>Limpiar</button>
      </SubmittedForm>
    </FormContainer>
  );
};

export default Form;
