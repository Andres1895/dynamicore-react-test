import styled from "styled-components";

export const FormContainer = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const StyledForm = styled.form`
  background-color: #d4dde6;
  padding: 3rem;
  border-radius: 20px;
  display: flex;
  flex-direction: column;
  height: 10rem;
  justify-content: space-evenly;
  align-items: baseline;
`;

export const SubmittedForm = styled.div<{ isVisible: boolean }>`
  display: ${(props) => (props.isVisible ? "flex" : "none")};
  flex-direction: column;
`;

export const Label = styled.label``;

export const ErrorLabel = styled(Label)`
  color: #c50606;
  font-weight: bold;
  font-size: small;
  margin: unset;
`;

export const Input = styled.div`
  display: flex;
  flex-direction: column;
`;
