import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const UserListContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 14rem;
  height: 75vh;
  overflow-y: scroll;
  background-color: #f0f2f4;
  padding: 0.5rem;
  margin-top: 2rem;
`;

export const UserInfo = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Label = styled.label`
  margin: 1rem;
`;

export const Title = styled.h3`
  display: flex;
  margin: 1rem;
`;

export const Button = styled.button`
  border: none;
  border-radius: 50px;
  cursor: pointer;
  min-height: 3rem;
  margin-top: 1rem;
  width: auto;
  background-color: #956aa6;
`;
