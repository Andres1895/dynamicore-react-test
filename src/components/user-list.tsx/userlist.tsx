import React, { useEffect, useState } from "react";
import { User, UserListProps } from "./types";
import { UserListContainer, Label, Title, UserInfo, Button, Container } from "./styles";

const UserList: React.FC<UserListProps> = ({ users }) => {
  const [sortedUsers, setSortedUsers] = useState<User[]>([]);
  const [sortOrder, setSortOrder] = useState<"asc" | "desc">("asc");

  useEffect(() => {
    if (users) {
      const sorted = [...users].sort((a, b) =>
        sortOrder === "asc"
          ? a.name.localeCompare(b.name)
          : b.name.localeCompare(a.name)
      );
      setSortedUsers(sorted);
    } else {
      setSortedUsers([]);
    }
  }, [users, sortOrder]);

  const toggleSortOrder = () => {
    setSortOrder((prevOrder) => (prevOrder === "asc" ? "desc" : "asc"));
  };

  const sortedButtonName =
    sortOrder === "asc" ? "Sort Descending" : "Sort Ascending";

  return (
    <Container>
      <Button onClick={toggleSortOrder}>{sortedButtonName}</Button>
      <UserListContainer>
        <UserInfo>
          <Title>Name</Title>
          <Title>Age</Title>
        </UserInfo>
        {sortedUsers.map((user: User, index: number) => (
          <UserInfo key={index}>
            <Label>{user.name}</Label>
            <Label>{user.age}</Label>
          </UserInfo>
        ))}
      </UserListContainer>
    </Container>
  );
};

export default UserList;
