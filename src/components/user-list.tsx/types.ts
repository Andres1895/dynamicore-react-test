export type User = {
  name: string;
  age: number;
};

export type UserListProps = {
  users: User[];
};
