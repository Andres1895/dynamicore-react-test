import { Routes, Route } from "react-router-dom";
import { UserListScreen, FormScreen } from "./screens";
import { Nav, Button, StyledLink } from "./styles";
import { useLocation } from 'react-router-dom';

const Tabs = () => {
    let location = useLocation();

  return (
    <div>
      <Nav>
        <Button>
          <StyledLink isActive={location.pathname === '/'} to="/">Home</StyledLink>
        </Button>
        <Button>
          <StyledLink isActive={location.pathname === '/form'} to="/form">Fill form</StyledLink>
        </Button>
      </Nav>

      <Routes>
        <Route path="/" element={<UserListScreen />} />
        <Route path="/form" element={<FormScreen />} />
      </Routes>
    </div>
  );
};

export default Tabs;
